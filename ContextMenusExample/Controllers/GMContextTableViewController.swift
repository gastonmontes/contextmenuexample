//
//  GMContextTableViewController.swift
//  ContextMenusExample
//
//  Created by Gaston Montes on 04/04/2020.
//  Copyright © 2020 Gaston Montes. All rights reserved.
//

import UIKit

private let kTableViewCellIdentifier = "AnimalCellIdentifier"

class GMContextTableViewController: UITableViewController, CellModelContextMenuActionDelegate {
    // MARK: - Vars.
    private var cellModels = [ CellModel ]()
    
    // MARK: - View life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.cellModels = CellModel.contextMenuCellModels(actionDelegate: self)
        
        self.setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Timer to change our data.
        Timer.scheduledTimer(withTimeInterval: 5.0, repeats: false) { [weak self] timer in
            let cellModel = self?.cellModels[3]
            let cellContextMenuData = cellModel?.cellContextMenuData
            
            cellContextMenuData?.contextMenuUpdate(title: "Expired")
            cellContextMenuData?.contextMenuSetActionsShareUpdateDelete()
            
            self?.tableView.reloadData()
        }
    }
    
    // MARK: - Setup functions.
    private func setupTableView() {
        self.tableView.allowsSelection = false
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: kTableViewCellIdentifier)
    }

    // MARK: - UITableViewDatasource implementation.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellModels.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = self.cellModels[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: kTableViewCellIdentifier, for: indexPath)
        cell.textLabel?.text = cellModel.cellData.menuDataTitle
        
        return cell
    }
    
    // MARK: - UITableViewDelegate.
    override func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let cellModel = self.cellModels[indexPath.row]
        let cellContextMenuData = cellModel.cellContextMenuData
        
        return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { suggestedActions in
            return UIMenu(title: cellContextMenuData.contextMenuTitle, children: cellContextMenuData.contextMenuActions)
        }
    }
    
    // MARK: - CellModelContextMenuActionDelegate implementation.
    func cellModelOnShareAction(cellData: CellData) {
        // Share your data.
    }
    
    func cellModelOnRenameAction(cellData: CellData) {
        // Rename your data.
    }
    
    func cellModelOnDeleteAction(cellData: CellData) {
        // Delete your data.
    }
    
    func cellModelOnUpdateAction(cellData: CellData) {
        // Update your data.
    }
}

//
//  CellContextMenuData.swift
//  ContextMenusExample
//
//  Created by Gaston Montes on 04/04/2020.
//  Copyright © 2020 Gaston Montes. All rights reserved.
//

import Foundation
import UIKit

protocol CellContextMenuDataActionDelegate: NSObject {
    func contextMenuOnShareAction()
    func contextMenuOnRenameAction()
    func contextMenuOnDeleteAction()
    func contextMenuOnUpdateAction()
}

class CellContextMenuData {
    // MARK: - Vars.
    private(set) var contextMenuTitle: String
    private(set) var contextMenuActions = [ UIAction ]()
    private(set) weak var contextMenuActionDelegate: CellContextMenuDataActionDelegate?
    
    // MARK: - Initialization.
    init(title: String) {
        self.contextMenuTitle = title
        
        self.contextMenuSetActionsShareRenameDelete()
    }
    
    // MARK: - Update functions.
    func contextMenuUpdate(title: String) {
        self.contextMenuTitle = title
    }
    
    func contextMenuUpdate(delegate: CellContextMenuDataActionDelegate) {
        self.contextMenuActionDelegate = delegate
    }
    
    // MARK: - Action functions.
    func contextMenuSetActionsShareRenameDelete() {
        let shareAction = UIAction(title: "Share") { [weak self] action in
            self?.contextMenuActionDelegate?.contextMenuOnShareAction()
        }
        
        let renameAction = UIAction(title: "Rename") { [weak self] action in
            self?.contextMenuActionDelegate?.contextMenuOnRenameAction()
        }
        
        let deleteAction = UIAction(title: "Delete") { [weak self] action in
            self?.contextMenuActionDelegate?.contextMenuOnDeleteAction()
        }
        
        self.contextMenuActions = [ shareAction, renameAction, deleteAction ]
    }
    
    func contextMenuSetActionsShareUpdateDelete() {
        let shareAction = UIAction(title: "Share") { [weak self] action in
            self?.contextMenuActionDelegate?.contextMenuOnShareAction()
        }
        
        let updateAction = UIAction(title: "Update") { [weak self] action in
            self?.contextMenuActionDelegate?.contextMenuOnUpdateAction()
        }
        
        let deleteAction = UIAction(title: "Delete") { [weak self] action in
            self?.contextMenuActionDelegate?.contextMenuOnDeleteAction()
        }
        
        self.contextMenuActions = [ shareAction, updateAction, deleteAction ]
    }
}

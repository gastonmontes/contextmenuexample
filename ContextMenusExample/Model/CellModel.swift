//
//  CellModel.swift
//  ContextMenusExample
//
//  Created by Gaston Montes on 04/04/2020.
//  Copyright © 2020 Gaston Montes. All rights reserved.
//

import Foundation

protocol CellModelContextMenuActionDelegate: NSObject {
    func cellModelOnShareAction(cellData: CellData)
    func cellModelOnRenameAction(cellData: CellData)
    func cellModelOnDeleteAction(cellData: CellData)
    func cellModelOnUpdateAction(cellData: CellData)
}

class CellModel: NSObject, CellContextMenuDataActionDelegate {
    // MARK: - Vars.
    private(set) var cellData: CellData
    private(set) var cellContextMenuData: CellContextMenuData
    private(set) weak var cellActionDelegate: CellModelContextMenuActionDelegate?
    
    // MARK: - Initialization.
    init(data: CellData, delegate: CellModelContextMenuActionDelegate) {
        self.cellData = data
        self.cellContextMenuData = CellContextMenuData(title: "Available")
        self.cellActionDelegate = delegate
        
        super.init()
        
        self.cellContextMenuData.contextMenuUpdate(delegate: self)
    }
    
    // MARK: - Statics functions.
    class func contextMenuCellModels(actionDelegate: CellModelContextMenuActionDelegate) -> [ CellModel ] {
        let menuDataList = CellData.animalsNamesData()
        var cellModels = [ CellModel ]()
        
        for menuData in menuDataList {
            let cellModel = CellModel(data: menuData, delegate: actionDelegate)
            cellModels.append(cellModel)
        }
        
        return cellModels
    }
    
    // MARK: - CellContextMenuDataActionDelegate implementation.
    func contextMenuOnShareAction() {
        self.cellActionDelegate?.cellModelOnShareAction(cellData: self.cellData)
    }
    
    func contextMenuOnRenameAction() {
        self.cellActionDelegate?.cellModelOnRenameAction(cellData: self.cellData)
    }
    
    func contextMenuOnDeleteAction() {
        self.cellActionDelegate?.cellModelOnDeleteAction(cellData: self.cellData)
    }
    
    func contextMenuOnUpdateAction() {
        self.cellActionDelegate?.cellModelOnUpdateAction(cellData: self.cellData)
    }
}

//
//  CellData.swift
//  ContextMenusExample
//
//  Created by Gaston Montes on 04/04/2020.
//  Copyright © 2020 Gaston Montes. All rights reserved.
//

import Foundation

class CellData {
    // MARK: - Vars.
    private(set) var menuDataTitle: String
    
    // MARK: - Initialization.
    init(title: String) {
        self.menuDataTitle = title
    }
    
    // MARK: - Statics functions.
    class func animalsNamesData() -> [ CellData ] {
        return [ CellData(title: "Dog"),
                 CellData(title: "Puppy"),
                 CellData(title: "Turtle"),
                 CellData(title: "Rabbit"),
                 CellData(title: "Parrot"),
                 CellData(title: "Cat"),
                 CellData(title: "Kitten"),
                 CellData(title: "Goldfish"),
                 CellData(title: "Mouse"),
                 CellData(title: "Tropical fish"),
                 CellData(title: "Hamster") ]
    }
}
